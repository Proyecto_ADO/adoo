																		
create database gestion;
																		
use gestion;
																		
create table horario(
id_hora INT(2) NOT NULL,
dia VARCHAR (10),
horai TIME,
horaf TIME,
constraint pk_chora primary key(id_hora)
);

																		
create table lugar(
id_lugar INT(4) NOT NULL,
nombre_lugar VARCHAR(60),
constraint pk_clu primary key (id_lugar)
);
																		
create table evento(
id_evento INT(4) NOT NULL,
nombre_ev VARCHAR(50),
desc_ev VARCHAR(255),
constraint pk_clu primary key (id_evento)
);
																		
create table departamento(
id_depto INT(2) NOT NULL,
nombre_depto VARCHAR(45),
porc_cred VARCHAR(10),
creditos FLOAT,
id_lugar INT(4),
constraint pk_cde primary key (id_depto),
constraint fk_fde foreign key(id_lugar) references lugar(id_lugar)
);
																		
create table club(
id_club INT(2) NOT NULL,
nombre_c VARCHAR(50),
constraint pk_club primary key (id_club)
);
																		
create table grupo(
id_grupo VARCHAR(6) NOT NULL,
turno VARCHAR(12),
id_lugar INT(4),
constraint pk_club primary key (id_grupo),
constraint fk_fgr foreign key(id_lugar) references lugar(id_lugar)
);
																		
create table profesor(
id_prof INT(4) NOT NULL,
nombre_p VARCHAR(70),
id_club INT(2),
constraint pk_prof primary key (id_prof),
constraint fk_cl foreign key(id_club) references club(id_club)
);
																		
create table ua(
id_ua INT(6) NOT NULL,
nombre_ua VARCHAR(60),
creditos_ua FLOAT,
nivel INT(1),
id_depto INT(2),
constraint pk_ua primary key (id_ua),
constraint fk_dep foreign key(id_depto) references departamento(id_depto)
);
																		
create table pua(
id_ua INT(6),
id_prof INT(4),
id_grupo VARCHAR(6),
id_hora INT(2),
constraint pk_pua primary key (id_ua,id_prof,id_grupo,id_hora),
constraint fk_puaua foreign key(id_ua) references ua(id_ua),
constraint fk_puap foreign key(id_prof) references profesor(id_prof),
constraint fk_puag foreign key(id_grupo) references grupo(id_grupo),
constraint fk_puah foreign key(id_hora) references horario(id_hora)
);
																		
create table det_evento(
id_evento INT(4),
id_hora INT(4),
id_lugar INT(4),
constraint pk_pev primary key (id_evento,id_hora,id_lugar),
constraint fk_det_ev1 foreign key(id_evento) references evento(id_evento),
constraint fk_det_ev2 foreign key(id_hora) references horario(id_hora),
constraint fk_det_ev3 foreign key(id_lugar) references lugar(id_lugar)
);
																		
create table det_club(
id_club INT(2),
id_hora INT(4),
id_lugar INT(4),
constraint pk_pcl primary key (id_club,id_hora,id_lugar),
constraint fk_det_cl1 foreign key(id_club) references club(id_club),
constraint fk_det_cl2 foreign key(id_hora) references horario(id_hora),
constraint fk_det_cl3 foreign key(id_lugar) references lugar(id_lugar)
);
																			
create table alumno(
id_alu INT(4) NOT NULL,
nombre_alu VARCHAR(60),
password VARCHAR(16),
correo VARCHAR(60),
telefono VARCHAR(14),
constraint pk_alu primary key (id_alu)
);
																			
create table publicacion(
id_publi INT(4) NOT NULL,
titulo_p VARCHAR(60),
contenido_p VARCHAR(255),
fecha_p DATETIME,
id_alu INT(4),
constraint pk_pub primary key (id_publi),
constraint fk_alup foreign key(id_alu) references alumno(id_alu)
);
																			
create table comentario(
id_comen INT(4) NOT NULL,
contenido_c VARCHAR(255),
fecha_c DATETIME,
id_alu INT(4),
id_publi INT(4),
id_comen1 INT(4),
constraint pk_com primary key (id_comen,id_publi,id_alu,fecha_c),
constraint fk_alum foreign key(id_alu) references alumno(id_alu),
constraint fk_publ foreign key(id_publi) references publicacion(id_publi),
constraint fk_come foreign key(id_comen1) references comentario(id_comen)
);
