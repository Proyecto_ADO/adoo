package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class HorarioController extends HttpServlet{

  public HorarioDAO horarioDAO;
  public HttpSession session;

  public HorarioController(){
    super();
    try
    {
      horarioDAO = new HorarioDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Horario horario = horarioDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("horario", horario);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Horario> horarios = horarioDAO.read();
      session = req.getSession(false);
      session.setAttribute("horarios", horarios);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  /*
  * String nombre, String password, String email, String boleta,
  * String apellidoMaterno, String apellidoPaterno, Vector pub
  */
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Horario horario = horarioDAO.read(id);
    session = req.getSession(false);
    horarioDAO.update(id, new Horario(
      req.getParameter("dia"),
      req.getParameter("horaInicio"),
      req.getParameter("horaFin")
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  /*
  * String nombre, String password, String email, String boleta,
  * String apellidoMaterno, String apellidoPaterno, Vector pub
  */
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    horarioDAO.create(new Horario(
      req.getParameter("dia"),
      req.getParameter("horaInicio"),
      req.getParameter("horaFin")
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    horarioDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }
}
