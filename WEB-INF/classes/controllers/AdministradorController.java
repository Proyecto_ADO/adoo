package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class AdministradorController extends HttpServlet{

  public AdministradorDAO administradorDAO;
  public HttpSession session;

  public AdministradorController(){
    super();
    try
    {
      administradorDAO = new AdministradorDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Administrador administrador = administradorDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("administrador", administrador);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Administrador> administradores = administradorDAO.read();
      session = req.getSession(false);
      session.setAttribute("administradores", administradores);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  /*
  * String nombre, String password, String email, String boleta,
  * String apellidoMaterno, String apellidoPaterno, Vector pub
  */
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Administrador administrador = administradorDAO.read(id);
    session = req.getSession(false);
    administradorDAO.update(id, new Administrador(
      req.getParameter("nombre"),
      req.getParameter("password"),
      req.getParameter("email")
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  /*
  * String nombre, String password, String email, String boleta,
  * String apellidoMaterno, String apellidoPaterno, Vector pub
  */
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    administradorDAO.create(new Administrador(
      req.getParameter("nombre"),
      req.getParameter("password"),
      req.getParameter("email")
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    administradorDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
