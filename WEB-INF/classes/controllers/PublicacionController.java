package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class PublicacionController extends HttpServlet{

  public PublicacionDAO publicacionDAO;
  public HttpSession session;

  public PublicacionController(){
    super();
    try
    {
      publicacionDAO = new PublicacionDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Publicacion publicacion = publicacionDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("publicacion", publicacion);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Publicacion> publicaciones = publicacionDAO.read();
      session = req.getSession(false);
      session.setAttribute("publicaciones", publicaciones);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Publicacion publicacion = publicacionDAO.read(id);
    session = req.getSession(false);
    publicacionDAO.update(id, new Publicacion(
      req.getParameter("contenido"),
      req.getParameter("titulo"),
      req.getParameter("fecha"),
      publicacion.getAutor(),
      publicacion.getComentarios()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    publicacionDAO.create(new Publicacion(
      req.getParameter("contenido"),
      req.getParameter("titulo"),
      req.getParameter("fecha"),
      new Alumno(new (req.getParameter("nombre")),(req.getParameter("password")), (req.getParameter("email")),
      (req.getParameter(boleta)), (req.getParameter(apellidoPaterno), (req.getParameter(apellidoMaterno)) )
      );
      new Vector<Comentario>()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    PublicacionDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
