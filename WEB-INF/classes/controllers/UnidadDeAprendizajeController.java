package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class UnidadDeAprendizajeController extends HttpServlet{

  public UnidadDeAprendizajeDAO unidadDeAprendizajeDAO;
  public HttpSession session;

  public UnidadDeAprendizajeController(){
    super();
    try
    {
      unidadDeAprendizajeDAO = new UnidadDeAprendizajeDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      UnidadDeAprendizaje unidadDeAprendizajeDAO = unidadDeAprendizajeDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("UnidadDeAprendizaje", UnidadDeAprendizaje);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<UnidadDeAprendizaje> unidadesDeAprendizaje = unidadDeAprendizajeDAO.read();
      session = req.getSession(false);
      session.setAttribute("unidadDeAprendizaje", unidadesDeAprendizaje);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro

  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    UnidadDeAprendizaje unidadDeAprendizaje = unidadDeAprendizajeDAO.read(id);
    session = req.getSession(false);
    unidadDeAprendizajeDAO.update(id, new UnidadDeAprendizaje(
      req.getParameter("nombre"),
      req.getParameter("temario"),
      req.getParameter("creditos"),
      UnidadDeAprendizaje.getProfesores(),
      UnidadDeAprendizaje.getGrupos()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro

  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    unidadDeAprendizajeDAO.create(new UnidadDeAprendizaje(
      req.getParameter("nombre"),
      req.getParameter("temario"),
      req.getParameter("creditos"),
      new Vector<Profesor>(),
      new Vector<Grupo>()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    unidadDeAprendizajeDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }

}
