package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class EspacioExteriorController extends HttpServlet{

  public EspacioExteriorDAO espacioExteriorDAO;
  public HttpSession session;

  public EspacioExteriorController(){
    super();
    try
    {
      espacioExteriorDAO = new EspacioExteriorDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      EspacioExterior espacioExterior = espacioExteriorDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("espacioExterior", espacioExterior);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<EspacioExterior> espacioExterior = espacioExteriorDAO.read();
      session = req.getSession(false);
      session.setAttribute("espacioExterior", espacioExterior);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    EspacioExterior espacioExterior = espacioExteriorDAO.read(id);
    session = req.getSession(false);
    espacioExteriorDAO.update(id, new EspacioExterior(
      req.getParameter("ubicacion")
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro

  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    espacioExteriorDAO.create(new EspacioExterior(
      req.getParameter("ubicacion")
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    espacioExteriorDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }
}
