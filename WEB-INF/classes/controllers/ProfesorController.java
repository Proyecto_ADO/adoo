package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class ProfesorController extends HttpServlet{

  public ProfesorDAO alumnoDAO;
  public HttpSession session;

  public ProfesorController(){
    super();
    try
    {
      profesorDAO = new ProfesorDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Profesor profesor = profesorDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("profesor", profesor);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Profesor> profesor = profesorDAO.read();
      session = req.getSession(false);
      session.setAttribute("profesor", profesor);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Profesor profesor = profesorDAO.read(id);
    session = req.getSession(false);
    ProfesorDAO.update(id, new Profesor(
      req.getParameter("nombre"),
      req.getParameter("curriculum"),
      alumno.getUnidadesDeAprendizajeAsignadas(),
      alumno.getGruposAsignados()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    profesorDAO.create(new Profesor(
      req.getParameter("nombre"),
      req.getParameter("curriculum"),
      new Vector<UnidadDeAprendizaje>(),
      new Vector<Grupo>()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    ProfesorDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
