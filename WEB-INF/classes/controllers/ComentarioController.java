package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class ComentarioController extends HttpServlet{

  public ComentarioDAO comentarioDAO;
  public HttpSession session;

  public ComentarioController(){
    super();
    try
    {
      comentarioDAO = new ComentarioDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Comentario comentario = comentarioDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("comentario", comentario);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Comentario> comentarios = comentarioDAO.read();
      session = req.getSession(false);
      session.setAttribute("comentarios", comentarios);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Comentario comentario = comentarioDAO.read(id);
    session = req.getSession(false);
    comentarioDAO.update(id, new Comentario(
      req.getParameter("contenido"),
      req.getParameter("fecha"),
      comentario.getComentariosRealizados(),
      comentario.getAutor()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    comentarioDAO.create(new Comentario(
      req.getParameter("contenido"),
      req.getParameter("fecha"),
      new Vector<Comentario>(),
      new Alumno(
        new (req.getParameter("nombre")), (req.getParameter("password")), (req.getParameter("email")),
        (req.getParameter("boleta")), (req.getParameter("apellidoPaterno")), (req.getParameter("apellidoPaterno")))
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    comentarioDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
