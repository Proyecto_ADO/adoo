package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class EventoController extends HttpServlet{

  public EventoDAO eventoDAO;
  public HttpSession session;

  public EventoController(){
    super();
    try
    {
      eventoDAO = new EventoDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Evento evento = EventoDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("evento", evento);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Evento> eventos = eventoDAO.read();
      session = req.getSession(false);
      session.setAttribute("eventos", eventos);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Evento evento = eventoDAO.read(id);
    session = req.getSession(false);
    eventoDAO.update(id, new Evento(
      req.getParameter("nombre"),
      req.getParameter("descripcion"),
      req.getParameter("fecha"),
      evento.getLugar(),
      evento.getHora()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    eventoDAO.create(new Evento(
      req.getParameter("nombre"),
      req.getParameter("descripcion"),
      req.getParameter("fecha"),
      new Lugar(new String(req.getParameter("nombre")));
      new Horario(new Date((req.getParameter("Dia")),req.getParameter("hInicio")),new Date(req.getParameter("hFin")));
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    eventoDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
