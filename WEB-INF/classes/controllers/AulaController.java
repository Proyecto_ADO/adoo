package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class AulaController extends HttpServlet{

  public AulaDAO aulaDAO;
  public HttpSession session;

  public AulaController(){
    super();
    try
    {
      aulaDAO = new AulaDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Aula aula = aulaDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("aula", aula);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Aula> aulas = aulaDAO.read();
      session = req.getSession(false);
      session.setAttribute("aula", aula);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Aula aula = aulaDAO.read(id);
    session = req.getSession(false);
    aulaDAO.update(id, new Aula(
      req.getParameter("numeroSalon"),
      req.getParameter("numeroPiso"),
      req.getParameter("edificio"),
      aula.getGruposAsignados()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    aulaDAO.create(new Aula(
      req.getParameter("numeroSalon"),
      req.getParameter("numeroPiso"),
      req.getParameter("edificio"),
      new Vector<gruposAsignados>()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    aulaDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
