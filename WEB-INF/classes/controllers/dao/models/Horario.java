package models;

import java.util.*;
import java.sql.Date;

public class Horario
{
  private int id;
  private String dia;
  private Date horaInicio;
  private Date horaFin;

  public Horario(Date inicio, Date fin, String dia)
  {
    horaInicio = inicio;
    horaFin = fin;
    this.dia = dia;
  }

  public String obtenerHorario()
  {
    return horaInicio + " " + horaFin;
  }

  public String getDia()
  {
    return dia;
  }

  public int getId()
  {
      return id;
  }

  public void setId(int id)
  {
    this.id = id;
  }

  public Date getHoraInicio()
  {
    return horaInicio;
  }

  public Date getHoraFin()
  {
    return horaFin;
  }


  public void setHorario(Date inicio, Date fin)
  {
      horaInicio = inicio;
      horaFin = fin;
  }

  public void setHoraInicio(Date inicio)
  {
    horaInicio = inicio;
  }

  public void setHoraFin(Date fin)
  {
    horaFin = fin;
  }

  public void setDia(String dia)
  {
    this.dia = dia;
  }

}
