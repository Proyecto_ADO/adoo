package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class ClubController extends HttpServlet{

  public ClubDAO clubDAO;
  public HttpSession session;

  public ClubController(){
    super();
    try
    {
      clubDAO = new ClubDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Club club = clubDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("club", club);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Club> clubs = clubDAO.read();
      session = req.getSession(false);
      session.setAttribute("club", club);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Club club = clubDAO.read(id);
    session = req.getSession(false);
    clubDAO.update(id, new Club(
      club.getLugar(),
      req.getParameter("nombre"),
      req.getParameter("creditos"),
      club.getHora()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro
  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    clubDAO.create(new Club(
      new Lugar(new req.getParameter("nombre"));
      req.getParameter("nombre"),
      req.getParameter("creditos"),
      new Horario(new Date(req.getParameter("hInicio")), new Date(req.getParameter("hFin")));
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    clubDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
