package controller;

import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import dao.*;
import dao.models.*;

public class AlumnoController extends HttpServlet{

  public AlumnoDAO alumnoDAO;
  public HttpSession session;

  public AlumnoController(){
    super();
    try
    {
      alumnoDAO = new AlumnoDAO();
    }catch(SQLException sql)
    {
      sql.printStackTrace();
    }

  }

  //Obtener un registro o todos los registros
  protected void doGet(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id;
    if ((id = Integer.parseInt(req.getParameter("id"))))
    {
      Alumno alumno = alumnoDAO.read(id);
      session = req.getSession(false);
      session.setAttribute("alumno", alumno);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
    else
    {
      Vector<Alumno> alumnos = alumnoDAO.read();
      session = req.getSession(false);
      session.setAttribute("alumnos", alumnos);
      session.setMaxInactiveInterval(1000);
      res.sendRedirect("");
    }
  }

  //Editar un registro
  protected void doPost(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    int id = req.getParameter("id");
    Alumno alumno = alumnoDAO.read(id);
    session = req.getSession(false);
    alumnoDAO.update(id, new Alumno(
      req.getParameter("nombre"),
      req.getParameter("password"),
      req.getParameter("email"),
      req.getParameter("boleta"),
      req.getParameter("apellidoMaterno"),
      req.getParameter("apellidoPaterno"),
      alumno.getPublicacionesRealizadas(),
      alumno.getComentariosRealizados()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Crear un nuevo registro

  protected void doPut(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    alumnoDAO.create(new Alumno(
      req.getParameter("nombre"),
      req.getParameter("password"),
      req.getParameter("email"),
      req.getParameter("boleta"),
      req.getParameter("apellidoMaterno"),
      req.getParameter("apellidoPaterno"),
      new Vector<Publicacion>(),
      new Vector<Comentario>()
    ));
    session.setMaxInactiveInterval(1000);
    res.sendRedirect("");
  }

  //Eliminar un registro
  protected void doDelete(HttpServletRequest req, HttpServletResponse res)
  throws ServletException, IOException
  {
    session = req.getSession(false);
    alumnoDAO.delete(req.getParameter("id"));
    session.setMaxInactiveInterval(0);
    res.sendRedirect("");
  }


}
